using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using HighloadCup.Dating.DataAccess;
using HighloadCup.Dating.DataAccess.DataLoader;
using HighloadCup.Dating.DataAccess.Exceptions;
using HighloadCup.Dating.DataAccess.Repositories;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using Microsoft.Extensions.Logging;
using HighloadCup.Dating.WebApi.ValueProvders;
using HighloadCup.Dating.WebApi.Filters;

namespace HighloadCup.Dating.WebApi.Controllers
{
    //Для всех URL, не указанных в приведённом API ожидается ответ с кодом 404 и пустым телом.
    [Route("[controller]")]
    [Produces("application/json")]
    [AddHeader("Connection","keep-alive")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountRepository _accountsRepository;

        public AccountsController(IAccountRepository accountsRepository)
        {
            _accountsRepository = accountsRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAccounts()
        {
            var accounts = await _accountsRepository.GetAccountsAsync();

            return new JsonResult(accounts);             
        }

        [HttpGet("filter")]
        [SeparatedQueryString]
        [ValidateQueryStringAttribute]
        public async Task<IActionResult> GetAccountsByFilter([FromQuery]AccountFilterDto filter)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            var accounts = await _accountsRepository.GetAccountsAsync(filter);

            return Ok(new {Accounts = accounts});
        }

        [HttpGet("group")]
        [SeparatedQueryString]
        public async Task<IActionResult> GetAccountsByGroups([FromQuery]AccountGroupingFilterDto groupFilter)
        {        
            if(!ModelState.IsValid)
                return BadRequest();

            var groups = await _accountsRepository.GetAccountsByGroupAsync(groupFilter);
        
            return Ok(new {Groups = groups});
        }

        [HttpGet("{id:int}/recommend")]
        [ValidateQueryStringAttribute]
        public async Task<IActionResult> GetAccountRecommendations(int id, [FromQuery]AccountSuggestRecommendFilterDto filter)
        {          
            if(!ModelState.IsValid)
                return BadRequest();

            try
            {
                var recommendations = await _accountsRepository.GetAccountRecommendationsAsync(id, filter);
                return Ok(new {Accounts = recommendations});
            }
            catch (EntityNotFoundException)
            {                
                return NotFound();
            }
        }

        //Если в хранимых данных не существует пользователя с переданным id, то ожидается код 404 с пустым телом ответа.
        [HttpGet("{id:int}/suggest")]
        [ValidateQueryStringAttribute]
        public async Task<IActionResult> GetAccountSuggestions(int id, [FromQuery]AccountSuggestRecommendFilterDto filter)
        {            
            if(!ModelState.IsValid)
                return BadRequest();

            try
            {
                var suggestions = await _accountsRepository.GetAccountSuggestionsAsync(id, filter);
                return Ok(new {Accounts = suggestions});
            }
            catch (EntityNotFoundException)
            {                
                return NotFound();
            }

        }

        //В случае некорректных типов данных, наличия неизвестных ключей или нарушения уникальности нужно вернуть код 400 с пустым телом.
        [HttpPost("new")]
        public async Task<IActionResult> CreateAccount([FromBody]AccountCreateDto account)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            try
            {
                await _accountsRepository.CreateAccountAsync(account);
            }
            catch (NotUniqueValueException)
            {                
                return BadRequest();
            }
            
            return StatusCode(201, new {});
        }
              
        //Аналогично добавлению нового пользователя, в случае некорректных типов данных, наличия неизвестных ключей или нарушения уникальности нужно вернуть код 400 с пустым телом
        [HttpPost("{id:int}")]
        public async Task<IActionResult> UpdateAccount(int id, [FromBody]AccountUpdateDto account)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            try
            {
                await _accountsRepository.UpdateAccountAsync(id, account);
            }
            catch (EntityNotFoundException)
            {                
                return NotFound();
            }
            catch (NotUniqueValueException)
            {                
                return BadRequest();
            }

            return Accepted(new {});
        }

        //Если в теле запроса переданы неизвестные поля или типы значений неверны, нужно вернуть код 400 с пустым телом.
        [HttpPost("likes")]
        public async Task<IActionResult> CreateLikes([FromBody] LikeBatchDto batch)
        {
            if(!ModelState.IsValid)
                return BadRequest();

            try
            {
                await _accountsRepository.CreateLikesAsync(batch?.Likes);   
            }
            catch (EntityNotFoundException)
            {             
                return BadRequest();
            }

            return Accepted(new {});
        }
    }
}
