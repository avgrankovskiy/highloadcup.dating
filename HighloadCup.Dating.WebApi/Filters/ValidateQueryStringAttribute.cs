using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace HighloadCup.Dating.WebApi.Filters
{
    public class ValidateQueryStringAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var filter = actionContext.ActionArguments["filter"];
            var parameters = filter.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).Select(x=> x.Name.ToLower()).ToList();
            var queryParameters = actionContext.HttpContext.Request.Query.Keys.Where(x=> x.ToLower() != "query_id").ToList();
            var queryKeyValues = actionContext.HttpContext.Request.Query.Where(x=> x.Key.ToLower() != "query_id").ToList();

            if(queryKeyValues.Any(kv=> string.IsNullOrWhiteSpace(kv.Value)))
            {
                actionContext.Result = new BadRequestResult();               
            }

            if (queryParameters.Any(queryParameter => !parameters.Any(p => p == queryParameter.ToLower())))
            {
                actionContext.Result = new BadRequestResult();
            }
        }
    }
}