﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Buffering;
using HighloadCup.Dating.DataAccess;
using HighloadCup.Dating.DataAccess.DataLoader;
using HighloadCup.Dating.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace HighloadCup.Dating.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<MongoDataContext>(x => new MongoDataContext());
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddMvcCore().AddJsonFormatters().AddDataAnnotations()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(options=>{
                    options.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Error;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });              
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }        

            app.UseResponseBuffering();
            app.UseMvc();  

            InitDataContext(app, logger);
        }

        private void InitDataContext(IApplicationBuilder app, ILogger<Startup> logger)
        {
            logger.LogInformation("Begin of init MongoDataContext and loading data...");
            var context = app.ApplicationServices.GetService<MongoDataContext>();
            logger.LogInformation("End of init MongoDataContext and loading data...");
        }
    }
}
