FROM microsoft/dotnet:2.2-sdk as build
WORKDIR /app

COPY . .
RUN dotnet restore
RUN dotnet publish -c Release -o ./out

FROM ubuntu:xenial as runtime

RUN apt-get update -y \ 
    && apt-get install -y wget \ 
    && apt-get install -y dpkg

##ASP.NET CORE RUNTIME
RUN wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb \
    && dpkg -i packages-microsoft-prod.deb \
    && apt-get install -y apt-transport-https \
    && apt-get update -y \
    && apt-get install -y aspnetcore-runtime-2.2

ENV ASPNETCORE_VERSION 2.2.0

ENV ASPNETCORE_URLS=http://+:80 \
    # Enable detection of running in a container
    DOTNET_RUNNING_IN_CONTAINER=true

##MONGO##
RUN  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4 \
    && echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list \
    && apt-get update -y \
    && apt-get install -y mongodb-org

EXPOSE 27017

FROM runtime
WORKDIR /app
COPY --from=build /app/HighloadCup.Dating.WebApi/out ./

RUN apt-get update && apt-get install -y supervisor \
    && mkdir -p /var/log/supervisor
COPY Supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 8983
CMD ["/usr/bin/supervisord"]

