Solution for HighloadCup 2018. Details: https://highloadcup.ru. Project description: https://highloadcup.ru/media/condition/accounts_rules_en.html.
Application developed by ASP.NET Core REST-API with Mongo Db database, application hosted inside of Docker container.
/
Решение для HighloadCup 2018. Детали: https://highloadcup.ru. Условие задачи: https://highloadcup.ru/media/condition/accounts_rules.html.
Приложение представляет собой ASP.NET Core REST-API с базой данных MongoDb, приложение разворачивается в Docker контейнере.