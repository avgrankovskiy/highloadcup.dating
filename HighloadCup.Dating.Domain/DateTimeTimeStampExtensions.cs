using System;

namespace HighloadCup.Dating.Domain
{
    public static class DateTimeTimeStampExtensions
    {
        public static long ToTimeStamp(this DateTime date)
        {
            return new DateTimeOffset(date).ToUnixTimeSeconds();
        }

        public static DateTime FromTimeStamp(this long ts)
        {
            return DateTimeOffset.FromUnixTimeSeconds(ts).DateTime.ToLocalTime();
        }

        public static DateTime FromTimeStamp(this int ts)
        {
            return DateTimeOffset.FromUnixTimeSeconds(ts).DateTime.ToLocalTime();
        }

        public static DateTime ToStartDateOfYear(this int year)
        {
            return new DateTime(year,1,1);
        }        

        public static DateTime ToEndDateOfYear(this int year)
        {
            return new DateTime(year,1,1).AddYears(1).AddMilliseconds(-1);
        } 
    }
}