using System;
using System.ComponentModel.DataAnnotations;

namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class PremiumPeriod
    {
        public DateTime Start { get; set; }

        public DateTime Finish { get; set; }
    }
}