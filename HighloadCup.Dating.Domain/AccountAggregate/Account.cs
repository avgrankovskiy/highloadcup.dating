using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class Account
    {
        public int Id;

        public string Email { get; set; }

        public string Fname { get; set; }

        public string Sname { get; set; }

        public string Phone { get; set; }

        public string Sex { get; set; }

        public DateTime Birth { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public DateTime Joined { get; set; }

        public string Status { get; set; }

        public string[] Interests { get; set; }

        public PremiumPeriod Premium { get; set; }

        public Like[] Likes { get; set; }

        public SimilarAccount[] SimilarAccounts { get; set; }

        private readonly Dictionary<string, decimal> StatusCompatibilityMaxParts = new Dictionary<string, decimal>()
        {
          {RelationshipStatus.Free, 0.5m},
          {RelationshipStatus.ItsComplicated, 0.3m},
          {RelationshipStatus.InRelationship, 0.1m},
        };
        private const decimal InterestCompatibilityMaxPart = 0.30m;
        private const decimal AgeCompatibilityMaxPart = 0.20m;
        private const decimal PremiumCompatibilityMaxPart = 1m;
        private const decimal EmptyCompatibility = 0m;

        public decimal CalculateCompatibilityByStatus(string anoutherAccountStatus)
        {
            var compatibility = 0m;
            if(!StatusCompatibilityMaxParts.Keys.Contains(anoutherAccountStatus))
              return compatibility;

            return StatusCompatibilityMaxParts[anoutherAccountStatus];
        } 

        public decimal CalculateCompatibilityByInterests(string[] anoutherAccountInterests)
        {
            if(Interests != null && Interests.Any())
            {
              if(anoutherAccountInterests == null || !anoutherAccountInterests.Any())
                return EmptyCompatibility;

              var compatibility = EmptyCompatibility;
              var compatibilityPartForEachInterest = InterestCompatibilityMaxPart / Interests.Count();
              compatibility = Interests.Intersect(anoutherAccountInterests).Sum(x => compatibilityPartForEachInterest);

              return compatibility;
            }

            if(((Interests == null || !Interests.Any()) && (anoutherAccountInterests == null || !anoutherAccountInterests.Any())))          
              return InterestCompatibilityMaxPart;          

            return EmptyCompatibility;
        }        

        public decimal CalculateCompatibilityByAge(DateTime anoutherAccountBirth)
        {
            if(Birth.Year == anoutherAccountBirth.Year)
              return AgeCompatibilityMaxPart;

            var compatibility = Math.Abs(AgeCompatibilityMaxPart/(Birth.ToTimeStamp() - anoutherAccountBirth.ToTimeStamp()));

            return compatibility;
        } 

        public decimal CalculateCompatibility(Account anoutherAccount)
        {
            var compatibility = EmptyCompatibility;

            var compatibilityByInterests = CalculateCompatibilityByInterests(anoutherAccount.Interests);
            if(compatibilityByInterests == EmptyCompatibility)
              return EmptyCompatibility;

            var compatibilityByStatus = CalculateCompatibilityByStatus(anoutherAccount.Status);
            var compatibilityByAge = CalculateCompatibilityByAge(anoutherAccount.Birth); 

            var compatibilityByPremium = anoutherAccount.HasPremium ? PremiumCompatibilityMaxPart : EmptyCompatibility;

            compatibility = compatibilityByStatus + compatibilityByInterests + compatibilityByAge + compatibilityByPremium;

            return compatibility;
        }
      
        public decimal CalclulateSimilarity(Account anoutherAccount)
        {
            var similarity = 0.0m;  
            var currentAccountLikes = Likes;
            var anoutherAccountLikes = anoutherAccount.Likes;
                     
            if(currentAccountLikes == null || !currentAccountLikes.Any())
              return similarity;
          
            if(anoutherAccountLikes == null || !anoutherAccountLikes.Any())
              return similarity;          
          
            //Объединяем по лайкам, чтобы найти общие 
            var sameLikes = currentAccountLikes.Join(anoutherAccountLikes, 
              ca => ca.AccountId, 
              aa => aa.AccountId, 
              (ca, aa) => new {AccountId = ca.AccountId, CurrentAccountTs = ca.Ts.ToTimeStamp(), AnoutherAccountTs = aa.Ts.ToTimeStamp() });              
          
            //Если общих нет, то сразу 0
            if(sameLikes.Count() == 0)
              return similarity;

            //Для лайков у другого пользователя вычисляем среднее значение времени через группировку, если есть несколько лайков одному пользователю
            sameLikes =  sameLikes.GroupBy(x => new {x.AccountId, x.CurrentAccountTs} )
              .Select(g => new 
              {
                  AccountId = g.Key.AccountId,
                  CurrentAccountTs = (long)g.Average(x => x.CurrentAccountTs),
                  AnoutherAccountTs = (long)g.Average(x => x.AnoutherAccountTs)
              });             

            //Считаем итоговое значение по формуле    
            similarity = sameLikes.Select(l => l.CurrentAccountTs != l.AnoutherAccountTs 
              ? 1.0m/Math.Abs(l.CurrentAccountTs - l.AnoutherAccountTs)
              : 1m
              ).Sum();

            return similarity;
        }    

        public bool HasPremium
        {            
            get
            {
              var now = DateTime.Now;
              if(Premium != null && (Premium.Start <= now && Premium.Finish >= now))
                return true;

              return false;
            }
        } 
    }            
}