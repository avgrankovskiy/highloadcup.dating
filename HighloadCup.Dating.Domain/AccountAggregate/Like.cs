using System;
using System.ComponentModel.DataAnnotations;

namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class Like
    {
        public int AccountId { get; set; }

        public DateTime Ts { get; set; }
    }
}