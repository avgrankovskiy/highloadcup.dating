namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class SexType
    {
        public const string Male = "m";

        public const string Female = "f";
    }
}