namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class SimilarAccount
    {
        public int AccountId { get; set; }

        public decimal Similarity { get; set; }
    }
}