namespace HighloadCup.Dating.Domain.AccountAggregate
{
    public class RelationshipStatus
    {
        public const string Free = "свободны";
        public const string InRelationship = "заняты";
        public const string ItsComplicated = "всё сложно";
    }
}