using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.Validation;

namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountDto
    {
        public int Id { get; set; }   

        public string Email { get; set; }

        public string Fname { get; set; }

        public string Sname { get; set; }

        public string Phone { get; set; }

        public string Sex { get; set; }

        public long? Birth { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Status { get; set; }

        public PremiumPeriodDto Premium { get; set; }
    }
}