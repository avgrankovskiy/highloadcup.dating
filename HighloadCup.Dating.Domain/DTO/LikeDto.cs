using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.Validation;

namespace HighloadCup.Dating.Domain.DTO
{
    public class LikeDto
    {
        //Id другого аккаунта
        [RequiredInt]
        public int Id { get; set; }

        public long Ts { get; set; }
    }
}