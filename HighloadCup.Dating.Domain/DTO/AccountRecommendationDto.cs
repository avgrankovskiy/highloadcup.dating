namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountRecommendationDto
    {
        public string Email { get; set; }

        public int Id { get; set; }

        public string Status { get; set; }

        public string Sname { get; set; }

        public string Fname { get; set; }   

        public long Birth { get; set; }     

        public PremiumPeriodDto Premium { get; set; }     
    }
}