namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountSuggestionDto
    {
        public string Email { get; set; }

        public int Id { get; set; }

        public string Status { get; set; }

        public string Sname { get; set; }

        public string Fname { get; set; }    
    }
}