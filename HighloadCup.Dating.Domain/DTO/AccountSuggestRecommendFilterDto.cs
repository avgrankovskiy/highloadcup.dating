using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.Validation;

namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountSuggestRecommendFilterDto
    {
        [RequiredInt]
        [Range(1,int.MaxValue)]
        public int Limit { get; set; }

        public string Country { get; set; }

        public string City { get; set; }
    }
}