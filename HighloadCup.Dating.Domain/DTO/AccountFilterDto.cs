using System.Collections.Generic;
using System.Linq;

namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountFilterDto
    {
        private HashSet<string> _filterKeys;

        public int Limit { get; set; }

        public string Sex_eq { get; set; }

        public string Email_domain { get; set; }    

        public string Email_lt { get; set; }

        public string Email_gt { get; set; }

        public string Status_eq { get; set; }

        public string Status_neq { get; set; }

        public string Fname_eq { get; set; }

        public string[] Fname_any { get; set; }

        public int? Fname_null { get; set; }

        public string Sname_eq { get; set; }

        public string Sname_starts { get; set; }

        public int? Sname_null { get; set; }

        public string Phone_code { get; set; }

        public int? Phone_null { get; set; }

        public string Country_eq { get; set; }

        public int? Country_null { get; set; }

        public string City_eq { get; set; }

        public string[] City_any { get; set; }

        public int? City_null { get; set; }

        public int? Birth_lt { get; set; }

        public int? Birth_gt { get; set; }

        public int? Birth_year { get; set; }

        public string[] Interests_contains { get; set; }

        public string[] Interests_any { get; set; }

        public int[] Likes_contains { get; set; }

        public int? Premium_now { get; set; }

        public int? Premium_null { get; set; }

        private HashSet<string> FilterKeys
        {
            get
            {
                if(_filterKeys == null)
                {
                    _filterKeys = new HashSet<string>();
                    FillFilterKeys();
                }
                
                return _filterKeys;
            }            
        }
        
        public bool HasFilterKey(string key)
        {
            return FilterKeys.Contains(key);            
        }

        private void FillFilterKeys()
        {
            if(!string.IsNullOrWhiteSpace(Sex_eq))
                FilterKeys.Add("Sex");                

            if(!string.IsNullOrWhiteSpace(Email_domain) 
                ||!string.IsNullOrWhiteSpace(Email_lt)                
                ||!string.IsNullOrWhiteSpace(Email_gt))
                    FilterKeys.Add("Email");       

            if(!string.IsNullOrWhiteSpace(Status_eq) 
                ||!string.IsNullOrWhiteSpace(Status_neq))
                    FilterKeys.Add("Status");

            if(!string.IsNullOrWhiteSpace(Fname_eq) 
                || (Fname_any != null && Fname_any.Any())
                || Fname_null.HasValue)
                    FilterKeys.Add("Fname");              

            if(!string.IsNullOrWhiteSpace(Sname_eq) 
                || !string.IsNullOrWhiteSpace(Sname_starts)
                || Sname_null.HasValue)
                    FilterKeys.Add("Sname"); 

            if(!string.IsNullOrWhiteSpace(Phone_code) 
                || Phone_null.HasValue)
                    FilterKeys.Add("Phone");                     
                
            if(!string.IsNullOrWhiteSpace(Country_eq) 
                || Country_null.HasValue)
                    FilterKeys.Add("Country");          

            if(!string.IsNullOrWhiteSpace(City_eq) 
                || City_any != null && City_any.Any()
                || City_null.HasValue)
                    FilterKeys.Add("City");                               
                
             if(Birth_lt.HasValue 
                || Birth_gt.HasValue
                || Birth_year.HasValue)
                    FilterKeys.Add("Birth");                                

             if(Premium_now.HasValue
                || Premium_null.HasValue)
                    FilterKeys.Add("Premium");                                                                 
        }
    }
}