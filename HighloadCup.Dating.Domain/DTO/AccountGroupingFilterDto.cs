using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.Validation;

namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountGroupingFilterDto
    {
        [Range(1, 50)]
        public int Limit {get;set;}      
      
        [Range(-1, 1)]
        public int Order {get;set;}
      
        [Required]
        [MinLength(1)]
        [ValidGroupKeys]
        public string[] Keys {get;set;}
      
        public int? Id { get; set; }   

        public string Email { get; set; }

        public string Fname { get; set; }

        public string Sname { get; set; }

        public string Phone { get; set; }

        public string Sex { get; set; }

        public int? Birth { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public int? Joined { get; set; }

        public string Status { get; set; }

        public string Interests { get; set; }

        public int? Likes { get; set; }         
    }
}