using System.ComponentModel.DataAnnotations;

namespace HighloadCup.Dating.Domain.DTO
{
    public class PremiumPeriodDto
    {
        [Range(1514764800, long.MaxValue)]
        public long Start { get; set; }

        [Range(1514764800, long.MaxValue)]
        public long Finish { get; set; }
    }
}