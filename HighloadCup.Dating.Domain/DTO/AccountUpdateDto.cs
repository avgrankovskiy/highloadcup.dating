using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.Validation;

namespace HighloadCup.Dating.Domain.DTO
{
    public class AccountUpdateDto
    {
        [MaxLength(100)]
        [EmailAddress]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Fname { get; set; }

        [MaxLength(50)]
        public string Sname { get; set; }

        [MaxLength(16)]
        public string Phone { get; set; }

        //Строка "m" означает мужской пол, а "f" - женский
        [MaxLength(1)]
        [ValidSex]
        public string Sex { get; set; }

        //Снизу 01.01.1950 и сверху 01.01.2005
        [Range(-631152000,1104537600)]
        public long? Birth { get; set; }

        [MaxLength(50)]
        public string Country { get; set; }

        [MaxLength(50)]
        public string City { get; set; }

        //Дата регистрации в системе. Снизу ограничена 01.01.2011, сверху 01.01.2018
        [Range(1293840000,1514764800)]
        public long? Joined { get; set; }

        //Строка из следующих вариантов: "свободны", "заняты", "всё сложно"
        [ValidStatus]
        public string Status { get; set; }

        public string[] Interests { get; set; }

        public PremiumPeriodDto Premium { get; set; }

        public List<LikeDto> Likes { get; set; }             
    }
}