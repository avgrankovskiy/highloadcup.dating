using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HighloadCup.Dating.Domain.DTO
{
    public class LikeBatchDto
    {
        [Required]
        public List<LikeBatchedDto> Likes { get; set; }
    }
}