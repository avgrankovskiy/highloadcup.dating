using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.DTO;
using System.Reflection;
using System;

namespace HighloadCup.Dating.Domain.Validation
{
    public class RequiredIfCountryExistAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var countryProperty = validationContext.ObjectType.GetProperty("Country");

            if(countryProperty == null)
                throw new ApplicationException($"Country not found in type {validationContext.ObjectType}"
                    + $" for {validationContext.MemberName} with ${nameof(RequiredIfCountryExistAttribute)}");

            if(countryProperty.PropertyType != typeof(string))
            {
                throw new ApplicationException($"Country is not string in type {validationContext.ObjectType}"
                    + $" for {validationContext.MemberName} with ${nameof(RequiredIfCountryExistAttribute)}");                
            }

            var city = (string)value;
            var country = (string)countryProperty.GetValue(validationContext.ObjectInstance);
            if (!string.IsNullOrWhiteSpace(city) && string.IsNullOrWhiteSpace(country))
            {
                return new ValidationResult("Country can not be empty if city existed");
            }

            return ValidationResult.Success;
        }
    }
}