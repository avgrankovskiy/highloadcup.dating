using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.DTO;

namespace HighloadCup.Dating.Domain.Validation
{
    public class RequiredIntAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int? intValue = value as int?;
        
            if (intValue == 0 || intValue == null)
            {
                return new ValidationResult("Int value is required");
            }

            return ValidationResult.Success;
        }
    }
}