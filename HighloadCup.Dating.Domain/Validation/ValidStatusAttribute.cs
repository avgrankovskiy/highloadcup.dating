using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;

namespace HighloadCup.Dating.Domain.Validation
{
    public class ValidStatusAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null)
                return ValidationResult.Success;

            if(value is string)
            {
                if(string.IsNullOrWhiteSpace(value as string))
                    return ValidationResult.Success;

                string status = (string)value;
                if (status != RelationshipStatus.Free && status != RelationshipStatus.InRelationship && status != RelationshipStatus.ItsComplicated)
                {
                    return new ValidationResult($"Value of Status: {value} is not valid for account status");
                }

                return ValidationResult.Success;
            }

            return new ValidationResult($"Value of Status: {value} must be string");
        }
    }
}