using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;
using System.Linq;

namespace HighloadCup.Dating.Domain.Validation
{
    public class ValidGroupKeysAttribute : ValidationAttribute
    {        
        private readonly string[] _validKeys = new string[] {"sex", "status", "interests", "country", "city"};
     
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {        
            if(value == null)
              return new ValidationResult("Group keys are required");
        
            var groupKeys = value as string[];     
            if(groupKeys == null)
                throw new ApplicationException($"Property with {nameof(ValidGroupKeysAttribute)} is not {typeof(string[])}");     
                                 
            if(groupKeys.Where(k => !_validKeys.Contains(k)).Count() > 0)
              return new ValidationResult("Group keys contains not valid keys");                  
     
            return ValidationResult.Success;
        }
    }
}