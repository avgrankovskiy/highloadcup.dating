using System.ComponentModel.DataAnnotations;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;

namespace HighloadCup.Dating.Domain.Validation
{
    public class ValidSexAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(value == null)
                return ValidationResult.Success;

            if(value is string)
            {
                if(string.IsNullOrWhiteSpace(value as string))
                    return ValidationResult.Success;
                
                string sex = (string)value;
                if (sex != SexType.Male && sex != SexType.Female)
                {
                    return new ValidationResult($"Value of Sex: {value} is not valid for account sex");
                }

                return ValidationResult.Success;
            }

            return new ValidationResult($"Value of Sex: {value} must be string");
        }
    }
}