using HighloadCup.Dating.Domain.DTO;
using HighloadCup.Dating.Domain.AccountAggregate;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using HighloadCup.Dating.DataAccess.Exceptions;
using System.Collections.Generic;
using System;
using HighloadCup.Dating.Domain;
using MongoDB.Bson;
using HighloadCup.Dating.DataAccess.QueryBuilders;
using System.Dynamic;
using System.Threading.Tasks;

namespace HighloadCup.Dating.DataAccess.Repositories
{
    public class AccountRepository
        : IAccountRepository
    {
        private readonly MongoDataContext _context;

        public AccountRepository(MongoDataContext context)
        {
            _context = context;
        }
        
        public async Task<List<Account>> GetAccountsAsync()
        {
            return await _context.Accounts.Find<Account>(x => true).Limit(100).ToListAsync();
        }

        public async Task<List<AccountDto>> GetAccountsAsync(AccountFilterDto filter)
        {
            var queryBuilder = new AccountsFilterQueryBuilder(filter);
            var accountsQuery = _context.Accounts.AsQueryable();    

            var filterExpressions = queryBuilder.BuildFilterQuery();        
            filterExpressions.ForEach(e => accountsQuery = accountsQuery.Where(e));  

            var accounts = await accountsQuery 
                .OrderByDescending(x => x.Id)               
                .Take(filter.Limit)
                .ToListAsync();

            var accountsResult =
                accounts
                .Select(x => new AccountDto(){
                    Id = x.Id,
                    Email = x.Email,
                    Birth = filter.HasFilterKey(nameof(x.Birth)) ? (long?)x.Birth.ToTimeStamp() : null,
                    Country = filter.HasFilterKey(nameof(x.Country)) ? x.Country : null,
                    City = filter.HasFilterKey(nameof(x.City)) ? x.City : null,
                    Fname = filter.HasFilterKey(nameof(x.Fname)) ? x.Fname : null,
                    Sname = filter.HasFilterKey(nameof(x.Sname)) ? x.Sname : null,
                    Phone = filter.HasFilterKey(nameof(x.Phone)) ? x.Phone : null,                   
                    Premium = filter.HasFilterKey(nameof(x.Premium)) ? x.Premium != null
                    ? new PremiumPeriodDto()
                        {
                            Start = x.Premium.Start.ToTimeStamp(),
                            Finish = x.Premium.Finish.ToTimeStamp(),

                        } : null
                    : null,
                    Sex = filter.HasFilterKey(nameof(x.Sex)) ? x.Sex : null,
                    Status = filter.HasFilterKey(nameof(x.Status)) ? x.Status : null
                })
                .ToList();

            return accountsResult;
        }

        public async Task<List<Dictionary<string,object>>> GetAccountsByGroupAsync(AccountGroupingFilterDto filter)
        {
            var queryBuilder = new GroupFilterQueryBuilder(filter);                         
            var accountsQuery = _context.Accounts.Aggregate();

            var filterExpressions = queryBuilder.BuildFilterQuery();        
            filterExpressions.ForEach(e => accountsQuery = accountsQuery.Match(e));
            
            //Делаем группировку по ключам группировки, если есть группировка по интересам, то делаем Unwind массива с интересами
            IAggregateFluent<BsonDocument> bsonQuery = null;
            var unwindQuery = queryBuilder.BuildUnwindQuery();
            var groupQuery = queryBuilder.BuildGroupQuery();
            if(unwindQuery != null){
                bsonQuery = accountsQuery
                    .Unwind(unwindQuery)
                    .Group(groupQuery);
            }
            else{
                bsonQuery = accountsQuery.Group(groupQuery);
            }

            var groups = await bsonQuery.ToListAsync();

            //Сортируем по количеству, а потом и по другим ключам
            if(filter.Order == 1)
            {
                var orderedGroups = groups
                    .OrderBy(x => x["count"].AsInt32);

                foreach (var key in filter.Keys)                
                    orderedGroups = orderedGroups
                        .ThenBy(x => x["_id"].AsBsonDocument.Contains(key) 
                            ? x["_id"].AsBsonDocument[key].IsBsonNull 
                                ? null 
                                : x["_id"].AsBsonDocument[key].AsString 
                            : "");

                groups = orderedGroups.Take(filter.Limit).ToList();                  
            }               
            else if(filter.Order == -1)
            {
                var orderedGroups = groups
                    .OrderByDescending(x => x["count"].AsInt32);
                    
                foreach (var key in filter.Keys)                
                    orderedGroups = orderedGroups
                        .ThenByDescending(x => x["_id"].AsBsonDocument.Contains(key) 
                            ? x["_id"].AsBsonDocument[key].IsBsonNull 
                                ? null 
                                : x["_id"].AsBsonDocument[key].AsString 
                            : "");

                groups = orderedGroups.Take(filter.Limit).ToList();
            }
      
            //Разбираем сгруппиированные данные в плоский объект
            var denormilizedGroups = groups.Select(x => {
                var groupingDictionary = new Dictionary<string,object>();
                var doc = x["_id"].AsBsonDocument.AsEnumerable();
                foreach (var element in doc)
                {
                    if(!element.Value.IsBsonNull)                    
                        groupingDictionary.Add(element.Name, element.Value.AsString);
                    
                }                                   

                groupingDictionary.Add("count", x["count"].AsInt32);

                groupingDictionary = groupingDictionary.OrderByDescending(kv=>kv.Key).ToDictionary(kv=>kv.Key,kv=>kv.Value);
                
                return groupingDictionary;
            }).ToList();

            return denormilizedGroups;
        }

        public async Task<List<AccountRecommendationDto>> GetAccountRecommendationsAsync(int accountId, AccountSuggestRecommendFilterDto filter)
        {
            if(filter == null)
              throw new ArgumentNullException(nameof(filter));
              
            var account = await _context.Accounts.Find<Account>(x => x.Id == accountId).FirstOrDefaultAsync();

            if(account == null)
                throw new EntityNotFoundException();         

            var accountsQuery = _context.Accounts.AsQueryable();

            accountsQuery = accountsQuery.Where(x => x.Id != account.Id && x.Sex != account.Sex);

            if(!string.IsNullOrWhiteSpace(filter.Country))
                accountsQuery = accountsQuery.Where(x => x.Country == filter.Country);

            if(!string.IsNullOrWhiteSpace(filter.City))
                accountsQuery = accountsQuery.Where(x => x.City == filter.City);
            
            List<string> interests = new List<string>(); 
            if(account.Interests != null && account.Interests.Any())
                interests = account.Interests.ToList();

            var recommendations = await accountsQuery
                .Where(x=>x.Interests.Any(i => interests.Contains(i)))
                .ToListAsync();

            var recommendationsResult = recommendations
                .Select(x => new {Account = x, Compatibility = account.CalculateCompatibility(x)})
                .Where(x => x.Compatibility > 0)
                .OrderByDescending(x => x.Compatibility)          
                .Take(filter.Limit) 
                .OrderByDescending(x => x.Compatibility)
                .ThenBy(x=> x.Account.Id)  
                .Select(x => new AccountRecommendationDto()
                {
                    Id = x.Account.Id,
                    Email = x.Account.Email,
                    Fname = x.Account.Fname,
                    Sname = x.Account.Sname,
                    Birth = x.Account.Birth.ToTimeStamp(),
                    Status = x.Account.Status,
                    Premium = x.Account.Premium != null ?
                        new PremiumPeriodDto()
                        {
                            Start = x.Account.Premium.Start.ToTimeStamp(),
                            Finish = x.Account.Premium.Finish.ToTimeStamp()
                        }
                        : null
                })                             
                .ToList();                              

            return recommendationsResult;
        }
        
        public async Task<List<AccountSuggestionDto>> GetAccountSuggestionsAsync(int accountId, AccountSuggestRecommendFilterDto filter)
        {
            if(filter == null)
              throw new ArgumentNullException(nameof(filter));
              
            var account = await _context.Accounts.Find<Account>(x => x.Id == accountId).FirstOrDefaultAsync();

            if(account == null)
                throw new EntityNotFoundException();         

            var accountsQuery = _context.Accounts.AsQueryable();

            accountsQuery = accountsQuery.Where(x => x.Id != account.Id && x.Sex == account.Sex);

            if(!string.IsNullOrWhiteSpace(filter.Country))
                accountsQuery = accountsQuery.Where(x => x.Country == filter.Country);

            if(!string.IsNullOrWhiteSpace(filter.City))
                accountsQuery = accountsQuery.Where(x => x.City == filter.City);

            List<int> accountLikeIds = new List<int>(); 
            if(account.Likes != null && account.Likes.Any())
                accountLikeIds = account.Likes.Select(x => x.AccountId).Distinct().ToList();
                        
            //Выбираем лайки пользователей, отсортированных по убыванию похожести
            var similarAccountLikesResult = await accountsQuery
                .Where(x=>x.Likes.Any(l=> accountLikeIds.Contains(l.AccountId)))
                .ToListAsync();
            var similarAccountLikes = 
                similarAccountLikesResult
                .Select(x => new {Likes = x.Likes, Similarity = account.CalclulateSimilarity(x)})
                .Where(x => x.Similarity > 0 && x.Likes != null)
                .OrderByDescending(x=>x.Similarity)                
                .Take(filter.Limit).ToList();


            //Выбираем лайки, которых нет у искомого пользователя по каждому похожему пользователю
            var suggestedAccountIdsAndSimilarity = similarAccountLikes                
                .SelectMany(x => 
                    x.Likes
                        .Select(l => l.AccountId)
                        .Distinct()
                        .Except(accountLikeIds)
                        .Select(l => new {AccountId = l, Similarity = x.Similarity})
                        .OrderByDescending(l=> l.AccountId)
                        .ToArray())
                .Take(filter.Limit)
                .ToList();


            var suggestedAccountIds = suggestedAccountIdsAndSimilarity.Select(x => x.AccountId).ToList();

            //Собираем итоговую выборку
            var suggestedAccounts =  _context.Accounts.AsQueryable()
                .Where(x=> suggestedAccountIds.Contains(x.Id) && x.Id != account.Id)
                .ToList()
                .Join(suggestedAccountIdsAndSimilarity,sa=>sa.Id,sas=>sas.AccountId, (sa,sas)=>new {
                    Email = sa.Email,
                    Id = sa.Id,
                    Status = sa.Status,
                    Sname = sa.Sname,
                    Fname = sa.Fname,
                    Similarity = sas.Similarity
                })
                .OrderByDescending(x => x.Similarity)
                .ThenByDescending(x => x.Id)
                .Select(x=> new AccountSuggestionDto{
                    Email = x.Email,
                    Id = x.Id,
                    Status = x.Status,
                    Sname = x.Sname,
                    Fname = x.Fname
                })
                .ToList();
                          
            return suggestedAccounts;
        }
        
        public async Task CreateAccountAsync(AccountCreateDto account)
        {
            if(account == null)
                throw new ArgumentNullException(nameof(account));

            if(await _context.Accounts.AsQueryable().AnyAsync(x => x.Id == account.Id))
                throw new NotUniqueValueException(); 
            
            if(!string.IsNullOrWhiteSpace(account.Email) && await _context.Accounts.AsQueryable().AnyAsync(x => x.Email == account.Email))
                throw new NotUniqueValueException();  

            if(!string.IsNullOrWhiteSpace(account.Phone) && await _context.Accounts.AsQueryable().AnyAsync(x => x.Phone == account.Phone))
                throw new NotUniqueValueException();           

            var newAccount = new Account()
            {
                Id = account.Id,
                Birth = account.Birth.FromTimeStamp(),
                Country = account.Country,
                City = account.City,     
                Email = account.Email,
                Phone = account.Phone,
                Fname = account.Fname,
                Sname = account.Sname,
                Interests = account.Interests,
                Joined = account.Joined.FromTimeStamp(),
                Likes = account.Likes?.Select(x => new Like(){
                    AccountId = x.Id,
                    Ts = x.Ts.FromTimeStamp()
                }).ToArray(),
                Premium = account.Premium != null ? new PremiumPeriod
                {
                    Start = account.Premium.Start.FromTimeStamp(),
                    Finish = account.Premium.Finish.FromTimeStamp()
                } : null,
                Sex = account.Sex,
                Status = account.Status
            };

            await _context.Accounts.InsertOneAsync(newAccount);
        }

        public async Task UpdateAccountAsync(int id, AccountUpdateDto account)
        {
            if(account == null)
                throw new ArgumentNullException(nameof(account));

            if(!string.IsNullOrWhiteSpace(account.Email) && _context.Accounts.AsQueryable().Any(x=> x.Email == account.Email))
                throw new NotUniqueValueException();

            if(!string.IsNullOrWhiteSpace(account.Phone) && _context.Accounts.AsQueryable().Any(x => x.Phone == account.Phone))
                throw new NotUniqueValueException();

            var updateAccount = await _context.Accounts.Find<Account>(x => x.Id == id).FirstOrDefaultAsync();

            if(updateAccount == null)
                throw new EntityNotFoundException();

            if(account.Fname != null)
                updateAccount.Fname = account.Fname;
            if(account.Sname != null)
                updateAccount.Sname = account.Sname;
            if(!string.IsNullOrWhiteSpace(account.Sex))
                updateAccount.Sex = account.Sex;
            if(account.Country != null)
                updateAccount.Country = account.Country;
            if(account.City != null)                
                updateAccount.City = account.City;
            if(!string.IsNullOrWhiteSpace(account.Email))
                updateAccount.Email = account.Email;
            if(account.Phone != null)
                updateAccount.Phone = account.Phone;
            if(!string.IsNullOrWhiteSpace(account.Status))
                updateAccount.Status = account.Status;            
            if(account.Birth.HasValue)
                updateAccount.Birth = account.Birth.Value.FromTimeStamp();
            if(account.Joined.HasValue)
                updateAccount.Joined = account.Joined.Value.FromTimeStamp();    
            if(account.Interests != null)            
                updateAccount.Interests = account.Interests;

            if(account.Premium != null)
            {
                updateAccount.Premium = new PremiumPeriod()
                {
                    Start = account.Premium.Start.FromTimeStamp(),
                    Finish = account.Premium.Finish.FromTimeStamp()                        
                };
            }

            if(account.Likes != null)
            {
                updateAccount.Likes = account.Likes?.Select(x => new Like(){
                    AccountId = x.Id,
                    Ts = x.Ts.FromTimeStamp()
                }).ToArray();
            }

            await _context.Accounts.ReplaceOneAsync<Account>(x => x.Id == id, updateAccount);
        }

        public async Task CreateLikesAsync(List<LikeBatchedDto> likes)
        {
            if(likes == null)
                throw new ArgumentNullException(nameof(likes));

            if(!likes.Any())
                return;

            var likeesIds = likes.Select(x=> x.Likee).Distinct().ToArray();
            var likerIds = likes.Select(x=> x.Liker).Distinct().ToArray();

            var counts = await Task.WhenAll(_context.Accounts.CountDocumentsAsync(x=> likeesIds.Contains(x.Id)),
                _context.Accounts.CountDocumentsAsync(x=> likerIds.Contains(x.Id))) ;
            
            var countLikeesIds = counts[0];
            var countLikerIds =  counts[1];

            if(likeesIds.Length != countLikeesIds)
                throw new EntityNotFoundException();

            if(likerIds.Length != countLikerIds)
                throw new EntityNotFoundException();

            var updateModels = likes.GroupBy(x=> x.Liker)
                .Select(g => new 
                {
                    LikerAccountId = g.Key,
                    Likes = g.Select(a => new Like(){
                        AccountId = a.Likee,
                        Ts = a.Ts.FromTimeStamp()
                    })
                })
                .Select(
                    x => new UpdateOneModel<Account>(Builders<Account>.Filter.Where(a => a.Id == x.LikerAccountId),
                        Builders<Account>.Update.PushEach<Like>(e => e.Likes,x.Likes)))
                .ToList();

            await _context.Accounts.BulkWriteAsync(updateModels,new BulkWriteOptions(){
                BypassDocumentValidation = true
            });
        }
    }
}