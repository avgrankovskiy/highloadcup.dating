using System.Collections.Generic;
using HighloadCup.Dating.Domain.DTO;
using HighloadCup.Dating.Domain.AccountAggregate;
using System.Dynamic;
using System.Threading.Tasks;

namespace HighloadCup.Dating.DataAccess.Repositories
{
    public interface IAccountRepository
    {
        Task<List<Account>> GetAccountsAsync();

        Task<List<AccountDto>> GetAccountsAsync(AccountFilterDto filter);

        Task<List<Dictionary<string,object>>> GetAccountsByGroupAsync(AccountGroupingFilterDto filter);
      
        Task<List<AccountSuggestionDto>> GetAccountSuggestionsAsync(int accountId, AccountSuggestRecommendFilterDto filter);

        Task<List<AccountRecommendationDto>> GetAccountRecommendationsAsync(int accountId, AccountSuggestRecommendFilterDto filter);
        
        Task CreateAccountAsync(AccountCreateDto account);

        Task UpdateAccountAsync(int id,AccountUpdateDto account);

        Task CreateLikesAsync(List<LikeBatchedDto> likes);             
    }
}