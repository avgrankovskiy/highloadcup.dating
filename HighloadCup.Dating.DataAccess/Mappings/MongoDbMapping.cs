using HighloadCup.Dating.Domain.AccountAggregate;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace HighloadCup.Dating.DataAccess.Mappings
{
    public static class MongoDbMapping
    {

        public static void RegisterAllMappings()
        {
            RegisterAccountMapping();
            RegisterLikeMapping();
            RegisterPremiumMapping();            
        }


        private static void RegisterAccountMapping()
        {
            if(!BsonClassMap.IsClassMapRegistered(typeof(Account)))
                BsonClassMap.RegisterClassMap<Account>(cm =>  {
                    cm.AutoMap();  
                    cm.MapMember(x=> x.Likes).SetIgnoreIfNull(true);
                    cm.MapMember(x=> x.Interests).SetIgnoreIfNull(true);
                });
        }

        private static void RegisterLikeMapping()
        {
            if(!BsonClassMap.IsClassMapRegistered(typeof(Like)))
                BsonClassMap.RegisterClassMap<Like>(cm =>  {
                    cm.AutoMap(); 
                });
        }

        private static void RegisterPremiumMapping()
        {
            if(!BsonClassMap.IsClassMapRegistered(typeof(PremiumPeriod)))
                BsonClassMap.RegisterClassMap<PremiumPeriod>(cm =>  {
                    cm.AutoMap();
                });
        }
    }
}