using MongoDB.Bson.Serialization.Conventions;

namespace HighloadCup.Dating.DataAccess.Mappings
{
    public static class MongoDbConvention
    {
        public static void RegisterConventions()
        {
            var pack = new ConventionPack();
            pack.Add(new CamelCaseElementNameConvention());
            ConventionRegistry.Register("HighLoadCupDbConvention",pack,t => true);  
        }
    }
}