namespace HighloadCup.Dating.DataAccess
{
    public class MongoDbSettings
    {
        public const string DbName = "highloadcup_dating_db";

        public const string Server = "mongodb://localhost:27017";

        #if DEBUG
        public const string DataFilePath = "/home/andrey/gitProjects/highloadcup.dating/HighloadCup.Dating.DataAccess/Data/data.zip";  

        #endif

        #if !DEBUG

        public const string DataFilePath = "/tmp/data/data.zip";    

        #endif
    }
}