﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace HighloadCup.Dating.DataAccess.DataLoader
{
    public class JsonDataLoader
    {        
        public static string[] LoadAccountsData()
        {
            if(!File.Exists(MongoDbSettings.DataFilePath))
                throw new FileNotFoundException($"File with accounts data not found: {MongoDbSettings.DataFilePath}");

            var jsonArray = UnpackZipDataFile();
              
            return jsonArray;                     
        }

        private static string[] UnpackZipDataFile()
        {
            List<string> jsonArray = new List<string>();
            using (ZipArchive archive = ZipFile.OpenRead(MongoDbSettings.DataFilePath))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    if (entry.FullName.Contains("accounts", StringComparison.OrdinalIgnoreCase))
                    {
                        using(var entryStream = entry.Open())
                        {
                            using(var entryStreamReader = new StreamReader(entryStream))
                            {
                                jsonArray.Add(entryStreamReader.ReadToEnd());
                            }
                        }
                    }
                }
            }

            return jsonArray.ToArray();
        }
    }
}
