using System.Collections.Generic;
using HighloadCup.Dating.DataAccess.Mappings;
using HighloadCup.Dating.Domain.AccountAggregate;
using MongoDB.Bson;
using MongoDB.Driver;

namespace HighloadCup.Dating.DataAccess
{
    public class MongoDataContext
    {
        private readonly IMongoDatabase _database;

        public MongoDataContext()
        {
            //Порядок имеет значение, регистрацию конвенций нужно вызывать перед регистрацией маппингов
            //TODO: Чтобы использовать nameof для ключей лучше отключить CamelCase
            MongoDbConvention.RegisterConventions();
            MongoDbMapping.RegisterAllMappings();
    
            var client = new MongoClient(MongoDbSettings.Server);
            _database = client.GetDatabase(MongoDbSettings.DbName);    

            var initializer = new MongoDbInitializer(client, MongoDbSettings.DbName);            
            initializer.Initialize();        
        }

        public IMongoCollection<Account> Accounts => _database.GetCollection<Account>("accounts");
    }
}