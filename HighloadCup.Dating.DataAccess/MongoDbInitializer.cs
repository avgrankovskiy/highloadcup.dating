using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Linq;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.DataAccess.DataLoader;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using Newtonsoft.Json;
using HighloadCup.Dating.Domain;

namespace HighloadCup.Dating.DataAccess
{
    public class MongoDbInitializer
    {
        private readonly MongoClient _client;
        private readonly string _dbName;

        public MongoDbInitializer(MongoClient client, string dbName)
        {
            _client = client;
            _dbName = dbName;
        }

        public void Initialize()
        {
            
            var accountsStrings = JsonDataLoader.LoadAccountsData();

            var isExist = IsDatabaseExistAsync().GetAwaiter().GetResult(); 
            if(isExist)
                _client.DropDatabase(_dbName);  

            FillAccounts(accountsStrings);     
            //FillSimilarAccounts();
            CreateIndexes();
        }

        private void FillAccounts(string [] accountsStrings)
        {   
            foreach(var accountsData in accountsStrings)
            {
                var document = BsonSerializer.Deserialize<BsonDocument>(accountsData);
                var accountsBsonArray = document.GetValue("accounts", null).AsBsonArray;
                
                if(accountsBsonArray == null)
                    throw new DataAccessException("Not found account element in string with data");    

                var accountsMongo = _client.GetDatabase(_dbName).GetCollection<BsonDocument>("accounts");  
                foreach(var bson in accountsBsonArray)
                {
                    var doc = bson.AsBsonDocument;
                    doc.Add(new BsonElement("_id", doc["id"]));
                    doc.Remove("id");

                    if(doc.TryGetElement("joined", out BsonElement joined))
                    {
                         doc["joined"] =joined.Value.AsInt32.FromTimeStamp();
                    }

                    if(doc.TryGetElement("birth", out BsonElement birth))
                    {
                         doc["birth"] = birth.Value.AsInt32.FromTimeStamp();
                    }

                    if(doc.TryGetElement("premium", out BsonElement premium))
                    {
                         premium.Value.AsBsonDocument["start"] = premium.Value.AsBsonDocument["start"].AsInt32.FromTimeStamp();
                         premium.Value.AsBsonDocument["finish"] = premium.Value.AsBsonDocument["finish"].AsInt32.FromTimeStamp();
                    }
                    
                    if(doc.TryGetElement("likes", out BsonElement likes)){
                        if(likes.Value != null)
                        {
                            foreach(var like in likes.Value.AsBsonArray)
                            {
                                like.AsBsonDocument.Add(new BsonElement("accountId", like["id"]));
                                like["ts"] = (BsonDateTime)like["ts"].AsInt32.FromTimeStamp();
                                like.AsBsonDocument.Remove("id");
                            }
                        }
                    }                    
                }    

                accountsMongo.BulkWrite(accountsBsonArray.Select(x=> new InsertOneModel<BsonDocument>(x.AsBsonDocument)).ToArray());
            }
        }

        public void CreateIndexes()
        {
            var accounts = _client.GetDatabase(MongoDbSettings.DbName).GetCollection<Account>("accounts");

            var sexIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Sex)
                .Descending(x => x.Id));

            var countryIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Country)
                .Descending(x => x.Id));

            var cityIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.City)
                .Descending(x => x.Id));

            var birthIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Birth)
                .Descending(x => x.Id));

            var statusIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Status)
                .Descending(x => x.Id));  

            var emailIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Email)
                .Descending(x => x.Id));                  

            var phoneIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Phone)
                .Descending(x => x.Id));               

            var sNameIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Sname)
                .Descending(x => x.Id));   

            var fNameIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x => x.Fname)
                .Descending(x => x.Id)); 

            var likeIdsIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending("likes.accountId")
                .Descending(x => x.Id));      

            var interestsIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x=>x.Interests)
                .Descending(x => x.Id));
            
            var joinedIndexModel = new CreateIndexModel<Account>(Builders<Account>
                .IndexKeys                
                .Ascending(x=>x.Joined)
                .Descending(x => x.Id));                                                          
                                                                                                 
            accounts.Indexes.CreateOne(sexIndexModel);
            accounts.Indexes.CreateOne(countryIndexModel);
            accounts.Indexes.CreateOne(cityIndexModel);
            accounts.Indexes.CreateOne(birthIndexModel);
            accounts.Indexes.CreateOne(statusIndexModel);
            accounts.Indexes.CreateOne(emailIndexModel);
            accounts.Indexes.CreateOne(phoneIndexModel);
            accounts.Indexes.CreateOne(sNameIndexModel);
            accounts.Indexes.CreateOne(fNameIndexModel);
            accounts.Indexes.CreateOne(likeIdsIndexModel); 
            accounts.Indexes.CreateOne(interestsIndexModel);  
            accounts.Indexes.CreateOne(joinedIndexModel);                 
        }

        private void FillSimilarAccounts()
        {     
            var collection = _client.GetDatabase(MongoDbSettings.DbName).GetCollection<Account>("accounts");
            var accounts = collection.Find(x=>true).ToList();

            foreach (var account in accounts)
            {
                collection.FindOneAndUpdate(a=> a.Id == account.Id,Builders<Account>.Update
                .Set(a=>a.SimilarAccounts, GetSimilarAccounts(account,accounts)));
            }
        }

        private SimilarAccount[] GetSimilarAccounts(Account account, List<Account> accounts)
        {                       
            //Выбираем лайки пользователей, отсортированных по убыванию похожести
            var similarAccounts = 
                accounts
                .Where(x => x.Id != account.Id && x.Sex == account.Sex)
                .Select(x => new SimilarAccount {AccountId = x.Id, Similarity = account.CalclulateSimilarity(x)})
                .Where(x => x.Similarity > 0)
                .OrderByDescending(x=>x.Similarity)
                .ToArray();

            return similarAccounts;                              
        }

        private async Task<bool> IsDatabaseExistAsync()
        {
            using(var cursor =  await _client.ListDatabaseNamesAsync()){
                var dbNames = await cursor.ToListAsync();
                return dbNames.Contains(_dbName);                    
            }
        }
    }
}