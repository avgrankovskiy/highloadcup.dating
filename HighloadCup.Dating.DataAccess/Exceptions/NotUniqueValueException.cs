using System;

namespace HighloadCup.Dating.DataAccess.Exceptions
{
    public class NotUniqueValueException
        : ApplicationException
    {
        public NotUniqueValueException()
            :base()
        {
            
        }

        public NotUniqueValueException(string message)
            : base(message)
        {
            
        }
    }
}