using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HighloadCup.Dating.Domain;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace HighloadCup.Dating.DataAccess.QueryBuilders
{
    public class AccountsFilterQueryBuilder
    {
        private readonly AccountFilterDto _filter;
        
        public AccountsFilterQueryBuilder(AccountFilterDto filter)
        {
            _filter = filter;
        }

        public List<Expression<Func<Account, bool>>> BuildFilterQuery()
        {
            var expressions = new List<Expression<Func<Account, bool>>>();

            if(!string.IsNullOrWhiteSpace(_filter.Sex_eq))
                expressions.Add(x => x.Sex == _filter.Sex_eq);

            if(_filter.Birth_lt.HasValue)
            {
                var startDate = _filter.Birth_lt.Value.FromTimeStamp();
                expressions.Add(x => x.Birth < startDate);
            }         
            if(_filter.Birth_gt.HasValue)
            {
                var endDate = _filter.Birth_gt.Value.FromTimeStamp();
                expressions.Add(x => x.Birth > endDate);
            }
            if(_filter.Birth_year.HasValue)
            {
                var startDate = _filter.Birth_year.Value.ToStartDateOfYear();
                var endDate = _filter.Birth_year.Value.ToEndDateOfYear();
                expressions.Add(x => x.Birth >= startDate && x.Birth <= endDate);
            }                 

            if(!string.IsNullOrWhiteSpace(_filter.Status_eq))
                expressions.Add(x => x.Status == _filter.Status_eq);
            if(!string.IsNullOrWhiteSpace(_filter.Status_neq))
                expressions.Add(x => x.Status != _filter.Status_neq);

            if(!string.IsNullOrWhiteSpace(_filter.Country_eq))
                expressions.Add(x => x.Country == _filter.Country_eq);      
            if(_filter.Country_null == 0)
                expressions.Add(x => !string.IsNullOrEmpty(x.Country));
            else if(_filter.Country_null == 1)
                expressions.Add(x => string.IsNullOrEmpty(x.Country));

            if(!string.IsNullOrWhiteSpace(_filter.City_eq))
                expressions.Add(x => x.City == _filter.City_eq);    
            if(_filter.City_any != null && _filter.City_any.Any())
                expressions.Add(x => _filter.City_any.Contains(x.City));                   
            if(_filter.City_null == 0)
                expressions.Add(x => !string.IsNullOrEmpty(x.City));
            else if(_filter.City_null == 1)
                expressions.Add(x => string.IsNullOrEmpty(x.City));                                

            if(!string.IsNullOrWhiteSpace(_filter.Fname_eq))
                expressions.Add(x => x.Fname == _filter.Fname_eq);                
            if(_filter.Fname_any != null && _filter.Fname_any.Any())
                expressions.Add(x => _filter.Fname_any.Contains(x.Fname));  
            if(_filter.Fname_null == 0)
                expressions.Add(x => !string.IsNullOrEmpty(x.Fname));
            else if(_filter.Fname_null == 1)
                expressions.Add(x => string.IsNullOrEmpty(x.Fname));

            if(!string.IsNullOrWhiteSpace(_filter.Sname_eq))
                expressions.Add(x => x.Sname == _filter.Sname_eq);    
            if(!string.IsNullOrWhiteSpace(_filter.Sname_starts))
                expressions.Add(x => x.Sname.StartsWith(_filter.Sname_starts));  
            if(_filter.Sname_null == 0)
                expressions.Add(x => !string.IsNullOrEmpty(x.Sname));
            else if(_filter.Sname_null == 1)
                expressions.Add(x => string.IsNullOrEmpty(x.Sname));   

            if(!string.IsNullOrWhiteSpace(_filter.Email_domain))
                expressions.Add(x => x.Email.EndsWith("@"+_filter.Email_domain));
            if(!string.IsNullOrWhiteSpace(_filter.Email_lt))
                expressions.Add(x => Builders<BsonDocument>.Filter.Lt("email",_filter.Email_lt).Inject());
            if(!string.IsNullOrWhiteSpace(_filter.Email_gt))
                expressions.Add(x => Builders<BsonDocument>.Filter.Gt("email",_filter.Email_gt).Inject());                   

            if(!string.IsNullOrWhiteSpace(_filter.Phone_code))
                expressions.Add(x => x.Phone.Contains("(" + _filter.Phone_code + ")"));
            if(_filter.Phone_null == 0)
                expressions.Add(x => !string.IsNullOrEmpty(x.Phone));
            else if(_filter.Phone_null == 1)
                expressions.Add(x => string.IsNullOrEmpty(x.Phone));     

            if(_filter.Interests_contains != null && _filter.Interests_contains.Any())            
                expressions.Add(x => Builders<BsonDocument>.Filter.All("interests", _filter.Interests_contains).Inject());

            if(_filter.Interests_any != null && _filter.Interests_any.Any())            
                expressions.Add(x => x.Interests.Any(i => _filter.Interests_any.Contains(i))); 

            if(_filter.Likes_contains != null && _filter.Likes_contains.Any())            
                expressions.Add(x => Builders<BsonDocument>.Filter.All("likes.accountId", _filter.Likes_contains).Inject());       
            
            if(_filter.Premium_now == 1)            
            {
                var now = DateTime.Now;
                expressions.Add(x => x.Premium != null && x.Premium.Start <= now && x.Premium.Finish >= now);                             
            }      
            if(_filter.Premium_null == 0)            
                expressions.Add(x => x.Premium != null);                             
            if(_filter.Premium_null == 1)            
                expressions.Add(x => x.Premium == null);                                                                                                                                                    

            return expressions;                                                          
        }
    }
}