using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HighloadCup.Dating.Domain;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain.DTO;
using MongoDB.Bson;
using MongoDB.Driver;

namespace HighloadCup.Dating.DataAccess.QueryBuilders
{
    public class GroupFilterQueryBuilder
    {
        private readonly AccountGroupingFilterDto _filter;
        
        public GroupFilterQueryBuilder(AccountGroupingFilterDto filter)
        {
            _filter = filter;
        }

        public List<Expression<Func<Account, bool>>> BuildFilterQuery()
        {
            var expressions = new List<Expression<Func<Account, bool>>>();

            if(_filter.Id.HasValue)
                expressions.Add(x => x.Id == _filter.Id.Value);
            if(!string.IsNullOrWhiteSpace(_filter.Sname))
                expressions.Add(x => x.Sname == _filter.Sname);
            if(!string.IsNullOrWhiteSpace(_filter.Fname))
                expressions.Add(x => x.Fname == _filter.Fname);
            if(!string.IsNullOrWhiteSpace(_filter.Status))
                expressions.Add(x => x.Status == _filter.Status);
            if(!string.IsNullOrWhiteSpace(_filter.Sex))
                expressions.Add(x => x.Sex == _filter.Sex);
            if(!string.IsNullOrWhiteSpace(_filter.Email))
                expressions.Add(x => x.Email == _filter.Email);
            if(!string.IsNullOrWhiteSpace(_filter.Phone))
                expressions.Add(x => x.Phone == _filter.Phone);
            if(_filter.Birth.HasValue)      
            {
                var startDate = _filter.Birth.Value.ToStartDateOfYear();
                var endDate = _filter.Birth.Value.ToEndDateOfYear();
                expressions.Add(x => x.Birth >= startDate && x.Birth <= endDate);
            }          
            if(_filter.Joined.HasValue)      
            {
                var startDate = _filter.Joined.Value.ToStartDateOfYear();
                var endDate = _filter.Joined.Value.ToEndDateOfYear();
                expressions.Add((x => x.Joined >= startDate && x.Joined <= endDate));
            }
            if(!string.IsNullOrEmpty(_filter.Country))      
                expressions.Add((x => x.Country == _filter.Country));
            if(!string.IsNullOrEmpty(_filter.City))      
                expressions.Add((x => x.City == _filter.City));
            if(!string.IsNullOrEmpty(_filter.Interests))      
                expressions.Add((x => x.Interests.Contains(_filter.Interests)));  
            if(_filter.Likes.HasValue)      
                expressions.Add((x => x.Likes.Any(l => l.AccountId == _filter.Likes.Value)));  

            return expressions;                                                          
        }

        public Expression<Func<Account, object>> BuildUnwindQuery()
        {
            if(_filter.Keys.Contains("interests"))
                return x => x.Interests;

            return null;
        }

        public BsonDocument BuildGroupQuery()
        {
            var keys = _filter.Keys.ToDictionary(k=> k,k => $"${k}");
            var groupBson = 
                new BsonDocument() { 
                    { "_id", new BsonDocument(keys)}, 
                    { "count", new BsonDocument("$sum", 1) } 
                };

            return groupBson;
        }

        public SortDefinition<BsonDocument> BuildSortQuery()
        {
            SortDefinition<BsonDocument> sortBson =null;

            if(_filter.Order == 1)
            {
                sortBson = Builders<BsonDocument>.Sort.Ascending("count");
                foreach (var key in _filter.Keys)
                    sortBson = sortBson.Ascending(key);
                    
                
            }   
            else if(_filter.Order == -1)
            {
                sortBson = Builders<BsonDocument>.Sort.Descending("count");
                foreach (var key in _filter.Keys)
                    sortBson = sortBson.Descending(key);
            }           

            return sortBson;
        }
    }
}