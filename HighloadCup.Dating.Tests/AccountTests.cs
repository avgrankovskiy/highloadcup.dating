using Microsoft.VisualStudio.TestTools.UnitTesting;
using HighloadCup.Dating.Domain.AccountAggregate;
using System;
using System.Globalization;

namespace HighloadCup.Dating.Tests
{
    [TestClass]
    public class AccountTests
    {
      
        private Account GetAccount()
        {
            return AccountBuilder.GetAccount();
        }
      
                       
        [TestMethod]
        public void CalclulateSimilarity_AccountsHaveNotSameLikes_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0;
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithoutSameLikes();
          
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }
      
        [TestMethod]
        public void CalclulateSimilarity_LikesIsEmpty_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0;
            
            var suggestAccount = GetAccount().WithoutAnyLikes();
            var anoutherAccount = GetAccount();          
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }
      
        [TestMethod]
        public void CalclulateSimilarity_LikesIsNull_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0;
            
            var suggestAccount = GetAccount().WithNullLikes();
            var anoutherAccount = GetAccount();          
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }      
      
        [TestMethod]
        public void CalclulateSimilarity_LikesAnoutherAccountIsEmpty_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0;
            
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithoutAnyLikes();          
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }
      
        [TestMethod]
        public void CalclulateSimilarity_LikesAnoutherAccountIsNull_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0;
            
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithNullLikes();          
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }      
      
        [TestMethod]
        public void CalclulateSimilarity_AccountsHaveLikesOnlyWithDifferentTimeAndWithoutSeveralLikesToOneAccount_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0.130m;
            
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithSameLikesAndDifferentTimeAndWithoutSeveralLikesToOneAccount();     
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }

        [TestMethod]
        public void CalclulateSimilarity_AccountsHaveLikesOnlyWithDifferentTimeAndWithSeveralLikesToOneAccount_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 0.025127113257912800305071819m;
            
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithSameLikesAndDifferentTimeAndWithSeveralLikesToOneAccount();     
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }

        [TestMethod]
        public void CalclulateSimilarity_AccountsHaveLikesWithSameTimeAndAndWithoutSeveralLikesToOneAccount_ReturnsExpectedSimilarity()
        {
            var expectedSimilarity = 1.125m;
            
            var suggestAccount = GetAccount();
            var anoutherAccount = GetAccount().WithSameTimeAndAndWithoutSeveralLikesToOneAccount();     
            var actualSimilarity = suggestAccount.CalclulateSimilarity(anoutherAccount);
          
            Assert.AreEqual(expectedSimilarity, actualSimilarity);
        }

        [TestMethod]
        public void CalculateCompatibility_HaveNotSameInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithoutSameInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibility_HaveNotAnyInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount().WithoutAnyInterests();
            var anoutherAccount = GetAccount();     
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibility_AnoutherAccountHaveNotAnyInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithoutAnyInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibility_AccountsHaveNotAnyInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount().WithoutAnyInterests(); 
            var anoutherAccount = GetAccount().WithoutAnyInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreNotEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]   
        public void CalculateCompatibility_AccountsHaveSameInterestAndAnouherIsFreeAndSameBirth_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 1m;         
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithAllSameInterests().WithStatus(RelationshipStatus.Free);
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }

        [TestMethod]   
        public void CalculateCompatibility_AccountsHaveSameInterestAndAnouherIsComplicatedAndSameBirth_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.8m;         
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithAllSameInterests().WithStatus(RelationshipStatus.ItsComplicated);
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }

        [TestMethod]   
        public void CalculateCompatibility_AccountsHaveSameInterestAndAnouherIsInRelationsdAndSameBirth_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.6m;         
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithAllSameInterests().WithStatus(RelationshipStatus.InRelationship);
            var actualCompatibility = recommendAccount.CalculateCompatibility(anoutherAccount);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }        

        [DataTestMethod]
        [DataRow(0.5,RelationshipStatus.Free)]
        [DataRow(0.3,RelationshipStatus.ItsComplicated)]
        [DataRow(0.1,RelationshipStatus.InRelationship)]
        [DataRow(0,"Bad status")]
        public void CalculateCompatibilityByStatus_AnoutherAccountWithStatus_ReturnsExpectedCompatibility(double compatibility, string status)
        {         
            var expectedCompatibility = (decimal)compatibility;
            var recommendAccount = GetAccount(); 
            var anoutherAccount = GetAccount().WithStatus(status);     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByStatus(anoutherAccount.Status);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountsWithNullInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.3m;
            
            var recommendAccount = GetAccount().WithNullInterests(); 
            var anoutherAccount = GetAccount().WithNullInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibilityByInterests_AnoutherAccountWithNullInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount(); 
            var anoutherAccount = GetAccount().WithNullInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountWithNullInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0m;
            
            var recommendAccount = GetAccount().WithNullInterests(); 
            var anoutherAccount = GetAccount();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountHasOnlyOneSameInterest_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.1m;
            
            var recommendAccount = GetAccount(); 
            var anoutherAccount = GetAccount().WithOnlyOneSameInterest();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }      

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountHasSeveralSameInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.2m;
            
            var recommendAccount = GetAccount(); 
            var anoutherAccount = GetAccount().WithSeveralSameInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);
        }   

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountHasAllSameInterests_ReturnsExpectedCompatibility()
        {
            var expectedCompatibility = 0.3m;
            
            var recommendAccount = GetAccount(); 
            var anoutherAccount = GetAccount().WithAllSameInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }   

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountHasManyInterestsAndSeveralSame_ReturnsExpectedCompatibility()
        {
            var interests = new string[]{"Массаж","Ужин с друзьями","Обнимашки","Лень","Книги","Зима","Дача","Кино"};
            var expectedCompatibility = 0.0750m;
            
            var recommendAccount = GetAccount().WithInterests(interests); 
            var anoutherAccount = GetAccount().WithSeveralSameInterests();     
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }     

        [TestMethod]
        public void CalculateCompatibilityByInterests_AccountHasManyInterestsAndAllSame_ReturnsExpectedCompatibility()
        {
            var interests = new string[]{"Массаж","Ужин с друзьями","Обнимашки","Лень","Книги","Зима","Дача","Кино"};
            var expectedCompatibility = 0.3m;
            
            var recommendAccount = GetAccount().WithInterests(interests); 
            var anoutherAccount = GetAccount().WithInterests(interests);
            var actualCompatibility = recommendAccount.CalculateCompatibilityByInterests(anoutherAccount.Interests);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }      

        [DataTestMethod]
        [DataRow("0.2","18.12.1993")]
        [DataRow("0.0000000013760114475345961614","11.05.1989")]
        [DataRow("0.0000000033732712912282060532","01.02.1992")]   
        [DataRow("0.0000000003131842939805346065","23.09.1973")]    
        public void CalculateCompatibilityByAge_AccountsHaveDifferentBirth_ReturnsExpectedCompatibility(string compatibility, string birth)
        {
            var expectedCompatibility = Decimal.Parse(compatibility);
            var birthDate = DateTime.ParseExact(birth,"dd.MM.yyyy", CultureInfo.InvariantCulture);            
            
            var recommendAccount = GetAccount();
            var anoutherAccount = GetAccount().WithBirthDate(birthDate);
            var actualCompatibility = recommendAccount.CalculateCompatibilityByAge(anoutherAccount.Birth);
          
            Assert.AreEqual(expectedCompatibility, actualCompatibility);            
        }                                          
    }
}
