using Microsoft.VisualStudio.TestTools.UnitTesting;
using HighloadCup.Dating.Domain.AccountAggregate;
using HighloadCup.Dating.Domain;
using System;

namespace HighloadCup.Dating.Tests
{    
    public static class AccountBuilder
    {      
        public static Account GetAccount()
        {
            var account = new Account() {
              Id = 1,
              Sex = "m",              
              Status = "заняты",
              City = "Лиссабирск",
              Email = "ugmotewawerpe@email.com",
              Birth = 756181228.FromTimeStamp(),
              Likes = new Like[]
              {
                new Like()
                  {
                    AccountId = 20072,
                    Ts = 1464151869.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 26014,
                    Ts = 1472394955.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 28356,
                    Ts = 1477256390.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 6346,
                    Ts = 1476769007.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 7604,
                    Ts = 1470036340.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 1592,
                    Ts = 1469188254.FromTimeStamp()
                  },
                new Like()
                  {
                    AccountId = 2630,
                    Ts = 1482596912.FromTimeStamp()
                  }
              },
              Joined = 1441411200.FromTimeStamp(),
              Country = "Росания",
              Fname = "Егор",
              Interests = new string[] {
                "Обнимашки",
                "Массаж",
                "Ужин с друзьями"
              }
            };
                    
            return account;
        }
      
        public static Account WithoutSameLikes(this Account account)
        {                   
            account.Likes = new Like[]
            {
              new Like()
              {
                AccountId = 9580,
                Ts = 1501118720.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 18022,
                Ts = 1453092743.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 21460,
                Ts = 1519978444.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 29892,
                Ts = 1464151861.FromTimeStamp()
              }
            };

            return account;
        }  

        public static Account WithSameLikesAndDifferentTimeAndWithoutSeveralLikesToOneAccount(this Account account)
        {                   
            account.Likes = new Like[]
            {
              new Like()
              {
                AccountId = 7604,
                Ts = 1470036140.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 1592,
                Ts = 1469188244.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 20072,
                Ts = 1464151829.FromTimeStamp()
              }
            };

            return account;
        }

        public static Account WithSameLikesAndDifferentTimeAndWithSeveralLikesToOneAccount(this Account account)
        {                   
            account.Likes = new Like[]
            {
              new Like()
              {
                AccountId = 7604,
                Ts = 1470036140.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 7604,
                Ts = 1470016140.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 7604,
                Ts = 1470033140.FromTimeStamp()
              },              
              new Like()
              {
                AccountId = 20072,
                Ts = 1464151829.FromTimeStamp()
              }
            };

            return account;
        }                  
      
      public static Account WithSameTimeAndAndWithoutSeveralLikesToOneAccount(this Account account)
      {
            account.Likes = new Like[]
            {
              new Like()
              {
                AccountId = 7604,
                Ts = 1470036340.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 1592,
                Ts = 1469188244.FromTimeStamp()
              },
              new Like()
              {
                AccountId = 20072,
                Ts = 1464151829.FromTimeStamp()
              }
            };

            return account;        
      }

        public static Account WithoutAnyLikes(this Account account)
        {
            account.Likes = new Like[]{};
                    
            return account;
        } 
      
        public static Account WithNullLikes(this Account account)
        {
            account.Likes = null;
                    
            return account;
        }

        public static Account WithoutSameInterests(this Account account)
        {
            account.Interests = new string[]{"Путешествия","Настолки","Шахматы"};
                    
            return account;
        }      

        public static Account WithOnlyOneSameInterest(this Account account)
        {
            account.Interests = new string[]{"Массаж",};
                    
            return account;
        }         

        public static Account WithSeveralSameInterests(this Account account)
        {
            account.Interests = new string[]{"Обнимашки","Массаж",};
                    
            return account;
        }    

        public static Account WithAllSameInterests(this Account account)
        {
            account.Interests = new string[]{"Массаж","Ужин с друзьями","Обнимашки"};
                    
            return account;
        }      

        public static Account WithInterests(this Account account, string[] interests)
        {
            account.Interests = interests;
                    
            return account;
        }          

        public static Account WithoutAnyInterests(this Account account)
        {
            account.Interests = new string[]{};
                    
            return account;
        }   

        public static Account WithNullInterests(this Account account)
        {
            account.Interests = null;
                    
            return account;
        }

        public static Account WithBirthDate(this Account account, DateTime birth)
        {
            account.Birth = birth;
                    
            return account;
        }        

        public static Account WithStatus(this Account account, string status)
        {
            account.Status = status;
                    
            return account;
        }
    }
}
